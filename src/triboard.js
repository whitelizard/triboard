// 0: undefined direction
// 1: color 1
// 2: color 2
// 3: color 3
// 4: color 4
// -1: outside map
//
// Triangle indexes:
//   -----------
//   \   0  /   \
//    \   /   1  \
//     -----------

export const availablePieces = [
  [0, 1, 0, 2, 0, 3],
  [0, 1, 0, 3, 0, 2],
  [0, 1, 0, 2, 0, 4],
  [0, 1, 0, 4, 0, 2],
  [0, 1, 0, 4, 0, 3],
  [0, 1, 0, 3, 0, 4],
  [0, 4, 0, 2, 0, 3],
  [0, 2, 0, 4, 0, 3]
];

export const mapToBoard = map => {};

export const hasT = { 0: code => code % 2 === 1, 1: code => code > 1 };

export const neighbours = (board, pieces, trIx) => {
  // if (board)
};
// -> [0, 2, 0, 4, 0, -1]

export const tri1 = [[0, 2, 3], [2, 3, 3], [3, 3, 1]];
export const star1 = [[0, 0, 2, 0], [0, 3, 3, 1], [2, 3, 3, 0], [0, 1, 0, 0]];
export const round2 = [[0, 2, 3, 3], [2, 3, 3, 3], [3, 3, 3, 1], [3, 3, 1, 0]];
export const tri2 = [
  [0, 0, 0, 2, 3],
  [0, 0, 2, 3, 3],
  [0, 2, 3, 3, 3],
  [2, 3, 3, 3, 3],
  [3, 3, 3, 3, 1]
];
export const rect1 = [
  [0, 2, 3, 3, 3, 3],
  [2, 3, 3, 3, 3, 3],
  [3, 3, 3, 3, 3, 3],
  [3, 3, 3, 3, 3, 1],
  [3, 3, 3, 3, 1, 0]
];
export const rect2 = [
  [0, 0, 2, 3, 3, 3, 3, 3],
  [0, 2, 3, 3, 3, 3, 3, 3],
  [2, 3, 3, 3, 3, 3, 3, 3],
  [3, 3, 3, 3, 3, 3, 3, 1],
  [3, 3, 3, 3, 3, 3, 1, 0],
  [3, 3, 3, 3, 3, 1, 0, 0]
];
