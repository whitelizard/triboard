import React from "react";
import { availablePieces } from "./triboard";

// const stroke = "rgb(255,235,20)";
const stroke = "black";
const strokeWidth = 3;
const colors = [
  "white",
  "rgb(255,50,0)",
  "rgb(40,100,255)",
  "rgb(0,230,0)",
  "rgb(255,255,0)"
];

export const BoardPair = ({ code, style, pieces = [] }) => {
  // console.log(pieces);
  const [p1Ix, p2Ix] = pieces;
  return (
    <span style={style}>
      <svg height="200" width={200 * 1.73}>
        <g style={{ display: code % 2 === 1 ? "" : "none" }}>
          <polygon
            points="0,0 231,0 115,200"
            style={{
              fill: "white",
              // fill: "rgb(30,230,190)",
              stroke,
              strokeWidth
            }}
          />
        </g>
        <g style={{ display: code > 1 ? "" : "none" }}>
          <polygon
            points="231,0 346,200 115,200"
            style={{
              fill: "white",
              // fill: "rgb(60,230,90)",
              stroke,
              strokeWidth
            }}
          />
        </g>
        {p1Ix !== undefined &&
          code % 2 === 1 &&
          (() => {
            const [_1, c1, _2, c2, _3, c3] = availablePieces[p1Ix] || [
              0,
              0,
              0,
              0,
              0,
              0
            ];
            return (
              <g style={{ display: p1Ix !== undefined ? "" : "none" }}>
                <polygon
                  points="0,0 231,0 115,67"
                  style={{
                    fill: colors[c1],
                    stroke,
                    strokeWidth: 1
                  }}
                />
                <polygon
                  points="115,67 231,0 115,200"
                  style={{
                    fill: colors[c2],
                    stroke,
                    strokeWidth: 1
                  }}
                />
                <polygon
                  points="115,67 115,200, 0,0"
                  style={{
                    fill: colors[c3],
                    stroke,
                    strokeWidth: 1
                  }}
                />
              </g>
            );
          })()}
        {p2Ix !== undefined &&
          code > 1 &&
          (() => {
            const [_1, c1, _2, c2, _3, c3] = availablePieces[p2Ix] || [
              0,
              0,
              0,
              0,
              0,
              0
            ];
            return (
              <g style={{ display: p1Ix !== undefined ? "" : "none" }}>
                <polygon
                  points="231,0 346,200 231,133"
                  style={{
                    fill: colors[c1],
                    stroke,
                    strokeWidth: 1
                  }}
                />
                <polygon
                  points="346,200 115,200 231,133"
                  style={{
                    fill: colors[c2],
                    stroke,
                    strokeWidth: 1
                  }}
                />
                <polygon
                  points="231,0 231,133 115,200"
                  // points="115,67 115,200, 0,0"
                  style={{
                    fill: colors[c3],
                    stroke,
                    strokeWidth: 1
                  }}
                />
              </g>
            );
          })()}
      </svg>
    </span>
  );
};
