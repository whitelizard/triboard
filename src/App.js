import React from "react";
import * as R from "ramda";
// import { triboard } from "./triboard";
import "./App.css";
import { BoardPair } from "./BoardPair";
import { rect2 as board } from "./triboard";

const marginLeft = -328;
const mapIx = R.addIndex(R.map);

function App() {
  let count = 0;
  return (
    <div className="App">
      <header className="App-header" style={{ marginLeft }}>
        {mapIx((row, y) => {
          return mapIx((code, x) => {
            const piece = code === 3 ? count : undefined;
            console.log(code, count, piece);
            if (code === 3) count += 1;
            return (
              <>
                <BoardPair
                  code={code}
                  style={{
                    position: "absolute",
                    top: y * 200,
                    left: x * 231 + 115 * y
                  }}
                  pieces={[piece % 8, piece % 8]}
                />
                <span
                  style={{
                    display: "none",
                    position: "absolute",
                    top: y * 200 + 78,
                    left: x * 231 + 115 * y + 138
                  }}
                >
                  ({x},{y})
                </span>
              </>
            );
          })(row);
        })(board)}
      </header>
    </div>
  );
}

export default App;
